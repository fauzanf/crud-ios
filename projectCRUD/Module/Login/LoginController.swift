//
//  LoginController.swift
//  projectCRUD
//
//  Created by Fauzan on 25/10/19.
//  Copyright © 2019 Fauzan. All rights reserved.
//

import UIKit

class LoginController: BaseController {
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var presenter: LoginPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = LoginPresenter(delegate: self)

        self.usernameText.delegate = self as? UITextFieldDelegate
        self.passwordText.delegate = self as? UITextFieldDelegate
    }
    
    @IBAction func usernameReturn(_ sender: UITextField) {
        self.passwordText.becomeFirstResponder()
    }
    
    @IBAction func passwordReturn(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    @IBAction func onLogin(_ sender: Any) {
        let username: String = usernameText.text!
        let password: String = passwordText.text!
        
        presenter?.doLogin(username: username, password: password)
    }
}

extension LoginController: LoginDelegate {
    func didFailedLogin(message: String) {
        
    }
    
    func didSuccessLogin() {
        
    }
    
}
