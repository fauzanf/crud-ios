//
//  LoginPresenter.swift
//  projectCRUD
//
//  Created by Nabilla on 25/10/19.
//  Copyright © 2019 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit

protocol LoginDelegate {
    func didFailedLogin(message: String)
    func didSuccessLogin()
}

class LoginPresenter {
    var delegate: LoginDelegate
    
    init(delegate: LoginDelegate) {
        self.delegate = delegate
    }
    
    func doLogin(username: String, password: String) {
        let request = LoginRequest()
        request.password = password
        request.username = username
        
        BaseAPI.instance.request(ApiLogin.login(request: request), success: { (code, json)  in
            if (code == 200) {
                self.delegate.didSuccessLogin()
            } else {
                self.delegate.didFailedLogin(message: "Unknown Error")
            }
        }){ (error) in
            self.delegate.didFailedLogin(message: error ?? "")
        }
    }
    
}
