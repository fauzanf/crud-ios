//
//  LoginRequest.swift
//  projectCRUD
//
//  Created by Nabilla on 25/10/19.
//  Copyright © 2019 PT Intersolusi Teknologi Asia. All rights reserved.
//

import Foundation

class LoginRequest: BaseRequest {

    var username: String?
    var password: String?

    override func buildForParameterAPI() -> [String : AnyObject] {
        var parameters = super.buildForParameterAPI()
        
        parameters["username"] = username as AnyObject
        parameters["password"] = password as AnyObject
        
        return parameters
    }
}
