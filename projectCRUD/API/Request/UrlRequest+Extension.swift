//
//  UrlRequest+Extension.swift
//  projectCRUD
//
//  Created by Nabilla on 25/10/19.
//  Copyright © 2019 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit
import KeychainSwift

extension URLRequest {
    
    /// Base Parameter Header
    ///
    /// - Parameters:
    ///   - params: params of API
    ///   - parameterUrl: url of API
    mutating func baseHeader (
        withParameters params: [String: AnyObject] = [:]
        , withParameterUrl parameterUrl: String? = nil, withTokenAuth token: Bool = false, withJsonRequest json: Bool = false) {
        
        
        if json {
            self.setValue("application/json", forHTTPHeaderField: "Accept")
            self.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        if token {
            let keychain = KeychainSwift()
            self.addValue("Bearer \(keychain.get("token")!)", forHTTPHeaderField: "authorization")
        }
        self.addValue("mobile", forHTTPHeaderField: "device")
    }
}
