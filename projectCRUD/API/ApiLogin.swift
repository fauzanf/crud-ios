//
//  ApiLogin.swift
//  projectCRUD
//
//  Created by Nabilla on 25/10/19.
//  Copyright © 2019 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit
import Alamofire

enum ApiLogin: URLRequestConvertible {
    case login(request: LoginRequest)
    
    var path: String {
        switch self {
        case .login(_):
            // path of api
            return ""
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .login(_):
            return .post
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .login(let request):
            return request.buildForParameterAPI()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try BaseAPI.Endpoint.baseUrl.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        
        // uncomment this if want to add parameter
        // urlRequest.baseHeader(withParameters: parameter)
        return try JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
