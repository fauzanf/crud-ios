//
//  BaseRequest.swift
//  projectCRUD
//
//  Created by Nabilla on 25/10/19.
//  Copyright © 2019 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit

class BaseRequest: NSObject {
    func buildForParameterAPI() -> [String : AnyObject] {
        return [:]
    }
}
