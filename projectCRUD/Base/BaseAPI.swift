//
//  BaseAPI.swift
//  projectCRUD
//
//  Created by Nabilla on 25/10/19.
//  Copyright © 2019 PT Intersolusi Teknologi Asia. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireNetworkActivityLogger
import Reachability

typealias SuccessResponse = (Int?, JSON?) -> Void
typealias ErrorResponse = (String?) -> Void

class BaseAPI: NSObject {
    class Endpoint {
        static let baseUrl = "https://api.zohar.nefo-fusion.com/"
    }
    
    static let instance = BaseAPI()
    
    let reachability: Reachability? = Reachability()
    var alamoFireManager: SessionManager = SessionManager.default
    var req: Request?
    
    override init() {
        super.init()
        setAFconfig()
    }
    
    func setAFconfig() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30 // seconds
        configuration.timeoutIntervalForResource = 30
        self.alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    private func checkErrorResponse(exceptionError error: String? = nil, exception:@escaping ErrorResponse) {
        if let reachability = reachability {
            switch reachability.connection {
            case .none:
                exception("No Internet Connection")
            default:
                if let error = error {
                    exception(error)
                } else {
                    exception("Internal Server Error")
                }
            }
        }
    }
    
    func flushBeforeRequest(_ request: URLRequestConvertible, success:@escaping SuccessResponse, exception:@escaping ErrorResponse) {
        alamoFireManager.session.flush { }
        alamoFireManager.session.getAllTasks(completionHandler: { (task) in
            task.forEach{ $0.cancel() }
        })
        self.request(request, success: success, exception: exception)
    }
    
    func request(_ request: URLRequestConvertible, success:@escaping SuccessResponse, exception:@escaping ErrorResponse) {
        NetworkActivityLogger.shared.startLogging()
        
        req = alamoFireManager.request(request).responseJSON { response in
            guard case let .failure(error) = response.result else {
                self.handleDataResponse(response: response, success: success, exception: exception)
                self.invalidateAndConfigure()
                return
            }
            self.throwExceptionError(error: error, exception: exception)
            self.invalidateAndConfigure()
        }
    }
    
    private func handleDataResponse(response: DataResponse<Any>
        , success: @escaping SuccessResponse
        , exception:@escaping ErrorResponse) {
        
        let status_code = response.response?.statusCode
        
        if let value = response.result.value {
            let jsonValue = JSON(value)
            success(status_code, jsonValue)
        } else {
            checkErrorResponse(exception: exception)
        }
    }
    
    func invalidateAndConfigure() {
        self.alamoFireManager.session.finishTasksAndInvalidate()
        self.setAFconfig()
    }
    
    private func throwExceptionError(error: Error, exception:@escaping ErrorResponse) {
        if let error = error as? AFError {
            switch error {
            case .invalidURL(let url):
                print("Invalid URL: \(url) - \(error.localizedDescription)")
            case .parameterEncodingFailed(let reason):
                print("Failure Reason: \(reason)")
            case .multipartEncodingFailed(let reason):
                print("Failure Reason: \(reason)")
            case .responseValidationFailed(let reason):
                print("Failure Reason: \(reason)")
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    print("Downloaded file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    print("Content Type Missing: \(acceptableContentTypes)")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                case .unacceptableStatusCode(let code):
                    print("Response status code was unacceptable: \(code)")
                }
            case .responseSerializationFailed(let reason):
                print("Failure Reason: \(reason)")
            }
            print("Underlying error: \(String(describing: error.underlyingError))")
        } else if let error = error as? URLError {
            checkErrorResponse(exceptionError: "Poor or no internet connection", exception: exception)
            return
        } else {
            print("Unknown error: \(String(describing: error))")
        }
        
        checkErrorResponse(exception: exception)
    }
}
